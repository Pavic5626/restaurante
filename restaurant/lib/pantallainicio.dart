import 'package:flutter/material.dart';
import 'package:restaurant/pantallaCarta.dart';

class PantallaInicio extends StatefulWidget {
  @override
  _PantallaInicioState createState() => _PantallaInicioState();
}

class _PantallaInicioState extends State<PantallaInicio> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color.fromARGB(255, 186, 185, 182),
      body: Container(
        child: Stack(
          children: <Widget>[
            Positioned(
              child: Align(
                alignment: FractionalOffset.bottomRight,
                child: Container(
                  padding:
                      EdgeInsets.only(right: 15, left: 5, top: 50, bottom: 50),
                  decoration: BoxDecoration(
                      color: Color.fromARGB(255, 0, 0, 0),
                      borderRadius:
                          BorderRadius.only(topLeft: Radius.circular(200))),
                  child: RotatedBox(
                    quarterTurns: 3,
                    child: Padding(
                      padding: EdgeInsets.only(top: 10),
                      child: Text(
                        "La mejor comida de mar",
                        style: TextStyle(
                            color: Color.fromARGB(255, 255, 255, 255),
                            fontStyle: FontStyle.italic,
                            fontSize: 20,
                            letterSpacing: 5),
                      ),
                    ),
                  ),
                ),
              ),
            ),
            Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Center(
                  child: Image.asset(
                    "assets/img/ic_logo.png",
                    width: MediaQuery.of(context).size.width,
                    height: 400,
                  ),
                ),
                Padding(
                  padding: EdgeInsets.only(top: 100),
                ),
                RaisedButton(
                  shape: CircleBorder(),
                  padding: EdgeInsets.all(30),
                  color: Color.fromARGB(255, 0, 0, 0),
                  child: Icon(
                    Icons.chevron_right,
                    size: 30,
                    color: Color.fromARGB(255, 222, 217, 217),
                  ),
                  onPressed: () {
                    Navigator.of(context).pushReplacement(MaterialPageRoute(
                        builder: (BuildContext) => Pantallacarta()));
                  },
                )
              ],
            )
          ],
        ),
      ),
    );
  }
}
