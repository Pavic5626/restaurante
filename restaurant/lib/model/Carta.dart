class Carta {
  final int id;
  final String nombre;
  final String imagen;

  const Carta({required this.id, required this.nombre, required this.imagen});
}

final platos = [
  new Carta(id: 1, nombre: "Conocer Menu", imagen: "ic_menu.png"),
  new Carta(id: 2, nombre: "Donde Estamos", imagen: "ic_ubicacion.png"),
  new Carta(id: 3, nombre: "Realizar una reserva", imagen: "ic_reserva.png"),
  new Carta(id: 4, nombre: "Descuentos", imagen: "ic_descuento.png"),
  new Carta(id: 5, nombre: "Realiza un pago", imagen: "ic_pago.png"),
  new Carta(id: 6, nombre: "Pide un domicilio", imagen: "ic_domicilio.png"),
];
