import 'package:flutter/material.dart';
import 'package:restaurant/pantallainicio.dart';

import 'model/Carta.dart';

class Pantallacarta extends StatefulWidget {
  const Pantallacarta({Key? key}) : super(key: key);

  @override
  State<Pantallacarta> createState() => _PantallacartaState();
}

class _PantallacartaState extends State<Pantallacarta> {
  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 1,
      child: Scaffold(
        backgroundColor: Color.fromARGB(255, 220, 213, 213),
        appBar: AppBar(
          title: Text("Marinada & Vino",
              style: TextStyle(
                  color: Color.fromARGB(255, 255, 255, 255),
                  fontStyle: FontStyle.italic)),
          backgroundColor: Color.fromARGB(255, 13, 16, 96),
          elevation: 0,
          bottom: TabBar(tabs: <Widget>[
            new Tab(
              child: Padding(
                padding: EdgeInsets.only(left: 5, right: 5),
                child: Text("Principal",
                    style: TextStyle(
                        color: Color.fromARGB(255, 255, 255, 255),
                        fontStyle: FontStyle.italic)),
              ),
            )
          ]),
        ),
        drawer: menuLateral(),
        body: TabBarView(children: <Widget>[
          Container(
              padding: EdgeInsets.all(10),
              child: GridView.builder(
                  itemCount: platos.length,
                  gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                      crossAxisCount: 2,
                      childAspectRatio: MediaQuery.of(context).size.width /
                          (MediaQuery.of(context).size.height / 2),
                      crossAxisSpacing: 20,
                      mainAxisSpacing: 20),
                  itemBuilder: (context, index) {
                    return Container(
                        margin: EdgeInsets.all(5),
                        padding: EdgeInsets.all(5),
                        decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.circular(10),
                            boxShadow: [
                              BoxShadow(
                                  color: Color(0x000005cc),
                                  blurRadius: 30,
                                  offset: Offset(10, 10))
                            ]),
                        child: Column(children: <Widget>[
                          Image.asset("assets/img/" + platos[index].imagen),
                          Text(
                            platos[index].nombre,
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                        ]));
                  }))
        ]),
      ),
    );
  }
}

class menuLateral extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Drawer(
        child: ListView(children: <Widget>[
      UserAccountsDrawerHeader(
        accountName: Text("Marinada & Vino"),
        accountEmail: Text("marinada&vino@gmail.com"),
        decoration: BoxDecoration(color: Colors.grey),
      ),
      InkWell(
          child: new ListTile(
            title: Text("Inicio"),
            leading: Icon(Icons.home, color: Colors.black),
          ),
          onTap: () {
            Navigator.of(context).pushReplacement(
                MaterialPageRoute(builder: (BuildContext) => PantallaInicio()));
          }),
      InkWell(
          child: new ListTile(
            title: Text("Domicilio"),
            leading: Icon(Icons.bike_scooter),
          ),
          onTap: () {
            Navigator.of(context).pushReplacement(
                MaterialPageRoute(builder: (BuildContext) => PantallaInicio()));
          }),
      InkWell(
          child: new ListTile(
            title: Text("Ubicacion"),
            leading: Icon(Icons.location_city_sharp),
          ),
          onTap: () {
            Navigator.of(context).pushReplacement(
                MaterialPageRoute(builder: (BuildContext) => PantallaInicio()));
          }),
    ]));
  }
}
