import 'package:flutter/material.dart';
//import 'package:provider/provider.dart';
import 'package:restaurant/pantallaInicio.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        debugShowCheckedModeBanner: false,
        title: 'Restaurant Típika',
        theme: ThemeData(
          primarySwatch: Colors.blue,
        ),
        home: PantallaInicio());
  }
}
